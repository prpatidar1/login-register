

We are going to create a single HTML page where an end-user can:

Register

Login

View list of other users

Fields for a user:

email

username

password

first-name

last-name

gender

role (admin, operations, sales)

In this assignment, it is really important that you relate every functionality you are creating with the real world. Such as, how much a particular person in a particular role can view other people.

There are a couple of these cases, think wisely.
